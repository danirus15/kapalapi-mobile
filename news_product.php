<html>
<?php include "includes/head.php";?>
<body class="bg_event">
<?php include "includes/header.php";?>
 <div class="bg_float bg_event1"></div>
<div class="content">
	<div class="sky-carousel" id="kopi">
		<div class="sky-carousel-wrapper">
			<ul class="sky-carousel-container">
				<li>
					<img src="img/kopi1.png" alt="" />
					<div class="sc-content">
							<h2><span>Kapal Api Special</span></h2>
							<p>
								JNikmati variant Kapal Api Grande Java Latte yang dibuat dari campuran kopi bubuk tanpa ampas ditamba gula, susu bubuk dan coklat. Menghasilkan kopi yang Anda buat menjadi lebih kuat dan lebih mantap serta dengan aroma yang menggoda.
							</p>
					</div>
				</li>
				<li>
					<img src="img/kopi2.png" alt="" />
					<div class="sc-content">
							<h2><span>Kapal Api Mocca</span></h2>
							<p>
								Nikmati variant Kapal Api Grande Java Latte yang dibuat dari campuran kopi bubuk tanpa ampas ditamba gula, susu bubuk dan coklat. Menghasilkan kopi yang Anda buat menjadi lebih kuat dan lebih mantap serta dengan aroma yang menggoda.
							</p>
					</div>
				</li>
				<li>
					<img src="img/kopi3.png" alt="" />
					<div class="sc-content">
							<h2><span>Kapal Api Grande Java Latte</span></h2>
							<p>
								Nikmati variant Kapal Api Grande Java Latte yang dibuat dari campuran kopi bubuk tanpa ampas ditamba gula, susu bubuk dan coklat. Menghasilkan kopi yang Anda buat menjadi lebih kuat dan lebih mantap serta dengan aroma yang menggoda.
							</p>
					</div>
				</li>
				<li>
					<img src="img/kopi4.png" alt="" />
					<div class="sc-content">
							<h2><span>Kapal Api Susu</span></h2>
							<p>
								Nikmati variant Kapal Api Grande Java Latte yang dibuat dari campuran kopi bubuk tanpa ampas ditamba gula, susu bubuk dan coklat. Menghasilkan kopi yang Anda buat menjadi lebih kuat dan lebih mantap serta dengan aroma yang menggoda.
							</p>
					</div>
				</li>
				<li>
					<img src="img/kopi5.png" alt="" />
					<div class="sc-content">
							<h2><span>Kapal Api Grande Java Latte</span></h2>
							<p>
								Nikmati variant Kapal Api Grande Java Latte yang dibuat dari campuran kopi bubuk tanpa ampas ditamba gula, susu bubuk dan coklat. Menghasilkan kopi yang Anda buat menjadi lebih kuat dan lebih mantap serta dengan aroma yang menggoda.
							</p>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="clearfix pt20"></div>
</div>
<?php include "includes/footer.php";?>
</body>

</html>