<html>
<?php include "includes/head.php";?>
<body class="bg_coffee">
<?php include "includes/header.php";?>
<div class="bg_float bg_coffee1"></div>
<div class="content">
	<ul class="list_coffee_news">
		<li>
			<div class="pic imgLiquid"><img src="img/pic3.jpg" alt=""></div>
			<div class="text">
				<h1><a href="wikopedia_coffee_detail.php">Dahsyatnya Aroma dan Rasa Kopi Aceh</a></h1>	
				Aceh. Selain identik dengan kota serambi Mekah, juga dikenal sebagai salah satu kota di Indonesia dengan produksi kopi terbesar. Maka dari itu, jika kita mengaku sebagai pecinta kopi, Kopi Aceh Gayo ini patut sekali untuk dicicipi. Selain karena aromanya yang dahsyat,
				<div class="clearfix"></div>
				<div class="acc">
					<span>128</span>
					<img src="img/ico_love.png" alt="">
					<span>128</span>
					<img src="img/ico_view.png" alt="">
					<span>128 Comment</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</li>
		<li>
			<div class="pic imgLiquid"><img src="img/pic4.jpg" alt=""></div>
			<div class="text">
				<h1><a href="wikopedia_coffee_detail.php">Dahsyatnya Aroma dan Rasa Kopi Aceh</a></h1>	
				Aceh. Selain identik dengan kota serambi Mekah, juga dikenal sebagai salah satu kota di Indonesia dengan produksi kopi terbesar. Maka dari itu, jika kita mengaku sebagai pecinta kopi, Kopi Aceh Gayo ini patut sekali untuk dicicipi. Selain karena aromanya yang dahsyat,
				<div class="clearfix"></div>
				<div class="acc">
					<span>128</span>
					<img src="img/ico_love.png" alt="">
					<span>128</span>
					<img src="img/ico_view.png" alt="">
					<span>128 Comment</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</li>
	</ul>
	<div class="clearfix pt20"></div>
</div>
<?php include "includes/footer.php";?>
</body>

</html>