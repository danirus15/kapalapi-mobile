<html>
<?php include "includes/head.php";?>
<body class="bg_video">
<?php include "includes/header.php";?>
<div class="content content2">
	<div class="clearfix pt10"></div>
	<iframe width="100%" height="200" src="//www.youtube.com/embed/Zqz7VzdSWqY" frameborder="0" allowfullscreen></iframe>
	<div class="clearfix"></div>
	<div class="video_left detail ">
		<div class="pd10">
			<h4 class="ff pb10">Episode 3</h4>
			<b>Setelah pencarian panjang yang menegangkan.
		Inilah kelanjutan kisah Ario Bayu dan Anggika</b>
		</div>
		<div class="comment comment2">
			<form action="#" class="form_comment">
				<input type="text" placeholder="Add a comment or a reply here ..." class="input">
				<input type="submit" value="Send" class="btn">
			</form>
			<ul>
				<li>
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<div class="text">
						<span class="fl l_black">John Doe said:</span>
						<span class="fr l_black">2 hrs ago</span>
						<div class="clearfix"></div>
						Mantap mas, ngopi dulu sebelom balik, biar fokus.
					</div>
					<div class="clearfix"></div>
				</li>
				<li class="reply">
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<div class="text">
						<span class="fl l_black">John Doe said:</span>
						<span class="fr l_black">2 hrs ago</span>
						<div class="clearfix"></div>
						Mantap mas, ngopi dulu sebelom balik, biar fokus.
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<div class="text">
						<span class="fl l_black">John Doe said:</span>
						<span class="fr l_black">2 hrs ago</span>
						<div class="clearfix"></div>
						Mantap mas, ngopi dulu sebelom balik, biar fokus.
					</div>
					<div class="clearfix"></div>
				</li>
			</ul>
			
		</div>
	</div>
	<div class="video_right">
		<div class="">
			<a href="#">
				<div class="pic1">
					<img src="http://staging.waktunyakapalapi.com/assets/uploaded/coffeedio/thumb/cd_3_03_Nov_2014.jpg" alt="" class="img">
					<img src="img/btn_play.png" alt="" class="play">
				</div>
				<h5 class="bold pt5">Episode 1</h5>
				by Kapal Api
				<span class="block pt5">1.500 Viewer</span>
			</a>
			<a href="#">
				<div class="pic1">
					<img src="img/video1.jpg" alt="" class="img">
					<img src="img/btn_play.png" alt="" class="play">
				</div>
				<h5 class="bold pt5">Episode 1</h5>
				by Kapal Api
				<span class="block pt5">1.500 Viewer</span>
			</a>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php include "includes/footer.php";?>
</body>

</html>