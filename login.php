<html>
<?php include "includes/head.php";?>
<body class="bg_login">
<?php include "includes/header.php";?>
<div class="content">
	<h4 align="center">Daftar dengan Email</h4>
	<form action="#" class="form_login pt10">
		<input type="text" placeholder="Nama" class="input">
		<input type="text" placeholder="Email" class="input">
		<input type="text" placeholder="Username" class="input">
		<input type="password" placeholder="Password" class="input">
		<div class="clearfix pt5"></div>
		<strong>Tanggal Lahir</strong>
		<div class="clearfix pt5"></div>
		<select name="" id="" class="select3 input">
			<option value="">Bulan</option>
		</select>
		<select name="" id="" class="select3 input">
			<option value="">Tgl</option>
		</select>
		<select name="" id="" class="select3 input">
			<option value="">Tahun</option>
		</select>
		<div class="clearfix"></div>
		<label for="#" class="mr20">
			<input type="radio" name="gender"> Wanita
		</label>
		<label for="#">
			<input type="radio" name="gender"> Pria
		</label>
		<div class="clearfix pt10"></div>
		<input type="submit" value="Sign Up" class="btn">
	</form>
	<div class="clearfix pt10"></div>
	<span class="or">
		<span>or</span>
	</span>
	<a href="#" class="btn_sosmed">Connect with Facebook</a>
	<div class="clearfix pt10"></div>
	<a href="#" class="btn_sosmed btn_sosmed_tw">Connect with Twitter</a>
	<div class="clearfix pt20"></div>
</div>
<div class="footer_login">
	<?php include "includes/footer.php";?>
</div>
</body>

</html>