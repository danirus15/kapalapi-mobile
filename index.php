<html>
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="hl">
	<div id='slider'>
		<ul>
			<li><img src="img/hl.png" alt=""></li>
			<li><img src="img/hl.png" alt=""></li>
			<li><img src="img/hl.png" alt=""></li>
		</ul>
	</div>
	<nav>
		<a href='#' class='prev' onclick='slider.prev();return false;'><img class="nav_img" src="img/arrow_left.png" /></a>
		<a href='#' class='next' onclick='slider.next();return false;'><img class="nav_img2" src="img/arrow_right.png" /></a>
	</nav>
	<div class="bot"></div>
</div>
<div class="content">
	<div class="title">Coffee Moment</div>
	<div class="list_kotak">
		<a href="moment_detail.php">
			<div class="pic pic_1 imgLiquid">
				<img src="img/pic1.jpg" alt="">
			</div>
		</a>
		<div class="box_text">
			<div class="text">
				<a href="moment_user.php">
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<h6>Nugraha</h6>
					Ini dia temen berpikir, waktunya kapal api.
				</a>
			</div>
			<div class="clearfix"></div>
			<div class="acc">
				<a href="#">
					<span>128</span>
					<img src="img/ico_like.png" alt="">
				</a>
				<a href="#">
					<span>128</span>
					<img src="img/ico_view.png" alt="">
				</a>
				<a href="#">
					<span>128</span>
					<img src="img/ico_comment.png" alt="">
				</a>
			</div>
		</div>
	</div>
	<div class="list_kotak">
		<a href="moment_detail.php">
			<div class="pic pic_1 imgLiquid">
				<img src="img/pic1.jpg" alt="">
			</div>
		</a>
		<div class="box_text">
			<div class="text">
				<a href="moment_user.php">
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<h6>Nugraha</h6>
					Ini dia temen berpikir, waktunya kapal api.
				</a>
			</div>
			<div class="clearfix"></div>
			<div class="acc">
				<a href="#">
					<span>128</span>
					<img src="img/ico_like.png" alt="">
				</a>
				<a href="#">
					<span>128</span>
					<img src="img/ico_view.png" alt="">
				</a>
				<a href="#">
					<span>128</span>
					<img src="img/ico_comment.png" alt="">
				</a>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div align="center"><a href="#" class="btn_more">See More</a></div>
	<div class="clearfix pt30"></div>
	<div class="title">Coffee News</div>
	<div class="list_1">
		<div align="center"><img src="img/img1.png" alt="" class="pic"></div>
		<div class="clearfix pt15"></div>
		<a href="news_detail.php">House Of Coffee Kapal Api Di PRJ</a>
		<div class="clearfix"></div>
		<div class="info">
			<span>Kapal Api</span>
			<img src="img/ico_date.png" alt="">
			<span>Sat, 26/06/2013</span>
		</div>
		<div class="clearfix pt10"></div>
		Jalan - jalan ke PRJ, jangan lupa mampir di Booth Kapal Api di PRJ, ya? Kali ini Booth Kapal Api PRJ mengusung Tema “House of Coffee”. Kenapa? Karena di House Of Coffee Kapal Api PRJ, kita bisa belajar banyak hal mengenai Kopi. Mulai dari jenis-jenis Biji Kopi sampai cara-cara pengolahan dan penyajian kopi.
	</div>
	<div class="clearfix pt20"></div>
	<div align="center"><a href="#" class="btn_more">See More</a></div>
</div>
<?php include "includes/footer.php";?>
</body>

</html>