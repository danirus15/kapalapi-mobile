<html>
<?php include "includes/head.php";?>
<body class="bg_coffee">
<?php include "includes/header.php";?>
<div class="bg_float bg_coffee1"></div>
<div class="content">
	<div class="detail pt20">
		<div class="pic"><img src="img/pic3.jpg" alt=""></div>
		<div class="text">
			<h1>Dahsyatnya Aroma dan Rasa Kopi Aceh</h1>	
			Aceh. Selain identik dengan kota serambi Mekah, juga dikenal sebagai salah satu kota di Indonesia dengan produksi kopi terbesar. Maka dari itu, jika kita mengaku sebagai pecinta kopi, Kopi Aceh Gayo ini patut sekali untuk dicicipi. Selain karena aromanya yang dahsyat,
			<div class="clearfix"></div>
			<div class="acc">
				<span>128</span>
				<img src="img/ico_love.png" alt="">
				<span>128</span>
				<img src="img/ico_view.png" alt="">
				<span>128</span>
				<img src="img/ico_comment.png" alt="">
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="comment">
			<ul>
				<li>
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<div class="text">
						<span class="fl ">John Doe said:</span>
						<span class="fr ">2 hrs ago</span>
						<div class="clearfix"></div>
						Mantap mas, ngopi dulu sebelom balik, biar fokus.
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<div class="text">
						<span class="fl ">John Doe said:</span>
						<span class="fr ">2 hrs ago</span>
						<div class="clearfix"></div>
						Mantap mas, ngopi dulu sebelom balik, biar fokus.
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<div class="text">
						<span class="fl ">John Doe said:</span>
						<span class="fr ">2 hrs ago</span>
						<div class="clearfix"></div>
						Mantap mas, ngopi dulu sebelom balik, biar fokus.
					</div>
					<div class="clearfix"></div>
				</li>
			</ul>
			<form action="#" class="form_comment">
				<input type="text" placeholder="Add a comment or a reply here ..." class="input">
				<input type="submit" value="Send" class="btn">
			</form>
		</div>
	</div>
	<div class="clearfix pt20"></div>
</div>
<?php include "includes/footer.php";?>
</body>

</html>