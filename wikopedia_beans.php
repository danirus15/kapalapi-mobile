<html>
<?php include "includes/head.php";?>
<body class="bg_coffee">
<?php include "includes/header.php";?>
<div class="bg_float bg_coffee1"></div>
<div class="content">
	<ul class="list_bean">
		<li>
			<div>
				<img src="img/bean1.png" alt="" class="pic">
				<div class="text">
					<h1>Arabica</h1>
					Tahun ditemukan<br>
					1753 | Ethiopia<br>
					Bentuk biji datar
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix pt20"></div>
			<div>
				<img src="img/bean3.png" alt="" class="pic">
				<div class="text">
					Buah matang jatuh
					Membutuhkan 9 bulan untuk berbuah
					Berbuah pada suhu yang dingin
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix pt20"></div>
			<div>
				<img src="img/bean5.png" alt="" class="pic">
				<div class="text">
					Produksi biji lebih sedikit.
					Aroma sebelum disangrai seperti Blueberry.
				</div>
				<div class="clearfix"></div>
			</div>
		</li>
		<li>
			<div>
				<img src="img/bean2.png" alt="" class="pic">
				<div class="text">
					<h1>Arabica</h1>
					Tahun ditemukan<br>
					1753 | Ethiopia<br>
					Bentuk biji datar
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix pt20"></div>
			<div>
				<img src="img/bean4.png" alt="" class="pic">
				<div class="text">
					Buah matang jatuh
					Membutuhkan 9 bulan untuk berbuah
					Berbuah pada suhu yang dingin
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix pt20"></div>
			<div>
				<img src="img/bean6.png" alt="" class="pic">
				<div class="text">
					Produksi biji lebih sedikit.
					Aroma sebelum disangrai seperti Blueberry.
				</div>
				<div class="clearfix"></div>
			</div>
		</li>
	</ul>
	<div class="clearfix pt20"></div>
</div>
<?php include "includes/footer.php";?>
</body>

</html>