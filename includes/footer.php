<div id="footer">
	<div class="foot">
		Find us on:
		<div class="clearfix"></div>
		<div class="sosmed_bot">
			<a href="#">
				<img src="img/ico_fb.png" alt="">
				<span>KopiKapalApi</span>
			</a>
			<a href="#">
				<img src="img/ico_tw.png" alt="">
				<span>@KapalApi_ID</span>
			</a>
			<a href="#">
				<img src="img/ico_mail.png" alt="">
				<span>santos@kapalapi.co.id</span>
			</a>
		</div>
		<div class="clearfix"></div>
		Kapal Api &copy; 2014
	</div>
</div>
<script type="text/javascript" src="js/jquery.1.8.js"></script>
<script type="text/javascript" src="js/imgLiquid-min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$(".ico_menu").click(function (){
			$("#menu").slideDown(200);
		});
		$(".menu_back").click(function (){
			$("#menu").slideUp(200);
		});

		$(".imgLiquid").imgLiquid({fill:true});
	});

	$(document).ready(function () {


		$("#accordion .subjudul").click(function (){

			if($(this).find("a.item").hasClass("popularOver")){
				$("#accordion .subjudul ul").slideUp();
				$("#accordion .subjudul a").removeClass("popularOver");
			}
			else {
				$("#accordion .subjudul ul").slideUp();
				$("#accordion .subjudul a").removeClass("popularOver");
				$(this).find("ul").slideDown(200);
				$(this).find("a.item").addClass("popularOver");
			}

		});
	});
</script>
<script type="text/javascript" src="js/jquery.sky.carousel-1.0.2.min.js"></script>
<script type="text/javascript">
$(function() {
	$('#sky-carousel').carousel({
		itemWidth: 200,
		itemHeight: 100,
		distance: -20,
		selectedItemDistance: 0,
		selectedItemZoomFactor: 1,
		unselectedItemZoomFactor: 0.5,
		unselectedItemAlpha: 0.15,
		motionStartDistance: 100,
		topMargin: 30,
		gradientStartPoint: 0,
		gradientOverlayVisible:false,
		gradientOverlaySize: 100,
		selectByClick: false,
		preload:false,
		navigationButtonsVisible:false
	});
	$('#kopi').carousel({
		itemWidth: 200,
		itemHeight: 80,
		distance:20,
		selectedItemDistance: 0,
		selectedItemZoomFactor: 0.6,
		unselectedItemZoomFactor: 0.2,
		unselectedItemAlpha: 0.2,
		motionStartDistance:150,
		topMargin: 20,
		gradientStartPoint: 0,
		gradientOverlayVisible:false,
		gradientOverlaySize: 100,
		selectByClick: false,
		preload:false,
		navigationButtonsVisible:true
	});
	$('#wiki').carousel({
		itemWidth: 200,
		itemHeight: 80,
		distance: 30,
		selectedItemDistance: 0,
		selectedItemZoomFactor: 0.6,
		unselectedItemZoomFactor: 0.2,
		unselectedItemAlpha: 0.2,
		motionStartDistance:150,
		topMargin: 20,
		gradientStartPoint: 0,
		gradientOverlayVisible:false,
		gradientOverlaySize: 100,
		selectByClick: false,
		preload:false,
		navigationButtonsVisible:false
	});
});
</script>
<script type="text/javascript" src="js/swipe.js"></script>
<script>
	var sliderinfo = new Swipe(document.getElementById('sliderinfo'), {
      callback: function(e, pos) {
        
        var i = bullets0.length;
        while (i--) {
          bullets0[i].className = ' ';
        }
        bullets0[pos].className = 'on';

      }
    }),

    slider = new Swipe(document.getElementById('slider'), {
      callback: function(e, pos) {
        
        var i = bullets.length;
        while (i--) {
          bullets[i].className = ' ';
        }
        bullets[pos].className = 'on';

      }
    }),
    bullets = document.getElementById('position').getElementsByTagName('em'),
    
    slider2 = new Swipe(document.getElementById('slider2'), {
      callback: function(e, pos) {
        
        var i = bullets2.length;
        while (i--) {
          bullets2[i].className = ' ';
        }
        bullets2[pos].className = 'on';

      }
    }),
    bullets2 = document.getElementById('position2').getElementsByTagName('em')
</script>
