<html>
<?php include "includes/head.php";?>
<body class="bg_moment">
<?php include "includes/header.php";?>
<div class="bg_float bg_moment1"></div>
<div class="user_box">
	<div class="kiri">
		<div class="pic_user">
            <img src="img/user1.jpg" alt="">
            <label>
                <strong>+</strong> Change Photo
                <input type="file">
            </label>
        </div>
        <div class="btn_share">
        	Share Moment
        </div>
	</div>
	<div class="kanan">
		<div class="info">
            <h3>Rheinhart Manurung</h3>
            <strong>User Name</strong>
            Reinhart

            <strong>Addres</strong>
            Jakarta Selatan

            <strong>Email</strong>
            reinhart@email.com
        </div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="content">
	<div class="list_kotak list_kotak_besar">
		<a href="moment_detail.php">
			<div class="pic pic_1 imgLiquid">
				<img src="img/pic1.jpg" alt="">
			</div>
		</a>
		<div class="box_text">
			<div class="text">
				<a href="moment_user.php">
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<h6>Nugraha</h6>
					Ini dia temen berpikir, waktunya kapal api.
				</a>
			</div>
			<div class="clearfix"></div>
			<div class="acc">
				<a href="#">
					<span>128</span>
					<img src="img/ico_like.png" alt="">
				</a>
				<a href="#">
					<span>128</span>
					<img src="img/ico_view.png" alt="">
				</a>
				<a href="#">
					<span>128</span>
					<img src="img/ico_comment.png" alt="">
				</a>
			</div>
		</div>
	</div>
	<div class="list_kotak list_kotak_besar">
		<a href="moment_detail.php">
			<div class="pic pic_1 imgLiquid">
				<img src="img/pic1.jpg" alt="">
			</div>
		</a>
		<div class="box_text">
			<div class="text">
				<a href="moment_user.php">
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<h6>Nugraha</h6>
					Ini dia temen berpikir, waktunya kapal api.
				</a>
			</div>
			<div class="clearfix"></div>
			<div class="acc">
				<a href="#">
					<span>128</span>
					<img src="img/ico_like.png" alt="">
				</a>
				<a href="#">
					<span>128</span>
					<img src="img/ico_view.png" alt="">
				</a>
				<a href="#">
					<span>128</span>
					<img src="img/ico_comment.png" alt="">
				</a>
			</div>
		</div>
	</div>
	<div class="list_kotak list_kotak_besar">
		<a href="moment_detail.php">
			<div class="pic pic_1 imgLiquid">
				<img src="img/pic1.jpg" alt="">
			</div>
		</a>
		<div class="box_text">
			<div class="text">
				<a href="moment_user.php">
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<h6>Nugraha</h6>
					Ini dia temen berpikir, waktunya kapal api.
				</a>
			</div>
			<div class="clearfix"></div>
			<div class="acc">
				<a href="#">
					<span>128</span>
					<img src="img/ico_like.png" alt="">
				</a>
				<a href="#">
					<span>128</span>
					<img src="img/ico_view.png" alt="">
				</a>
				<a href="#">
					<span>128</span>
					<img src="img/ico_comment.png" alt="">
				</a>
			</div>
		</div>
	</div>


	<div class="clearfix pt20"></div>
	<div align="center"><a href="#" class="btn_more btn_load">Load More</a></div>
	<div class="clearfix pt20"></div>
	<a href="#" class="nav_top pic"><img src="img/arrow_up.png" alt=""></a>
</div>
<?php include "includes/footer.php";?>
</body>

</html>