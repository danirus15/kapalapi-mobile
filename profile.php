<html>
<?php include "includes/head.php";?>
<body class="bg_moment">
<?php include "includes/header.php";?>
<div class="bg_float bg_moment1"></div>
<div class="user_box">
	<div class="kiri">
		<div class="pic_user imgLiquid">
            <img src="img/user1.jpg" alt="">
            <label>
                <strong>+</strong> Change Photo
                <input type="file">
            </label>
        </div>
        <div class="link_user">
        	<a href="moment_add.php">Add Moment</a>
        	<a href="profile.php">Edit Profile</a>
        </div>
	</div>
	<div class="kanan">
		<div class="info">
            <h3>Rheinhart Manurung</h3>
            <strong>User Name</strong>
            Reinhart

            <strong>Addres</strong>
            Jakarta Selatan

            <strong>Email</strong>
            reinhart@email.com
        </div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="profile_edit">
	<h3>Photo</h3>
	<div class="clearfix"></div>
	<div class="pic imgLiquid"><img src="img/user1.jpg" alt=""></div>
	<h5>Upload Profile Picture</h5>
	You can upload a JPG or PNG file (file size limit is 1 MB)
	<br><br>
	<label class="btn_upload">
		Upload Foto
		<input type="file">
	</label>
	<div class="clearfix pt30"></div>
	<div class="fl w45p">
		<strong>First Name</strong>
		<input type="text" class="input">
	</div>
	<div class="fr w45p">
		<strong>Last Name</strong>
		<input type="text" class="input">
	</div>
	<div class="clearfix pt10"></div>
	<strong>Email*</strong>
	<input type="text" class="input">
	<div class="clearfix pt10"></div>
	<strong>Address*</strong>
	<input type="text" class="input">
	<div class="clearfix pt10"></div>
	<strong>Contact/Phone</strong>
	<input type="text" class="input">
	<div class="clearfix pt10"></div>
	<strong>Location*</strong>
	<input type="text" class="input">
	<div class="clearfix pt10"></div>
	<strong>About Me</strong>
	<textarea class="input textarea"></textarea>
	<div class="clearfix pt10"></div>
	<input type="submit" value="Save Profile" class="btn_upload fr">
	<div class="clearfix"></div>
</div>
<div class="clearfix pt20"></div>
<?php include "includes/footer.php";?>
</body>

</html>