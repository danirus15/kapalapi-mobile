<html>
<?php include "includes/head.php";?>
<body class="bg_moment">
<?php include "includes/header.php";?>
<div class="bg_float bg_moment1"></div>
<div class="content">

	<div class="moment_detail">
		<div class="foto">
			<span class="date">1 minute ago</span>
			<div class="pic_detail">
				<img src="img/moment1.jpg" alt="">
			</div>
			<div class="share">
				<a href="#"><img src="img/share_tw.png" alt=""></a>
				<a href="#"><img src="img/share_fb.png" alt=""></a>
				<a href="#"><img src="img/share_gplus.png" alt=""></a>
				<a href="#"><img src="img/share_p.png" alt=""></a>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="info_user">
			<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
			<div class="info_k">
				<h4>Nugraha</h4>
				Ini dia temen berpikir, waktunya kapal api.
				<div class="clearfix"></div>
				<div class="acc">
					<a href="#">
						<span>128</span>
						<img src="img/ico_like.png" alt="">
					</a>
					<a href="#">
						<span>128</span>
						<img src="img/ico_view.png" alt="">
					</a>
					<a href="#">
						<span>128</span>
						<img src="img/ico_comment.png" alt="">
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="comment">
			<ul id="comment_container">
            </ul>
	            <p> Please <a href="http://staging.waktunyakapalapi.com/index.php/signup/login" data-target="" class="btn">Login to comment</a></p><a href="http://staging.waktunyakapalapi.com/index.php/signup/login" data-target="" class="btn">
	        </a>
        </div>


		<div class="comment">
			<ul>
				<li>
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<div class="text">
						<span class="fl l_black">John Doe said:</span>
						<span class="fr l_black">2 hrs ago</span>
						<div class="clearfix"></div>
						Mantap mas, ngopi dulu sebelom balik, biar fokus.
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<div class="text">
						<span class="fl l_black">John Doe said:</span>
						<span class="fr l_black">2 hrs ago</span>
						<div class="clearfix"></div>
						Mantap mas, ngopi dulu sebelom balik, biar fokus.
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<span class="user pic imgLiquid"><img src="img/user1.jpg" alt=""></span>
					<div class="text">
						<span class="fl l_black">John Doe said:</span>
						<span class="fr l_black">2 hrs ago</span>
						<div class="clearfix"></div>
						Mantap mas, ngopi dulu sebelom balik, biar fokus.
					</div>
					<div class="clearfix"></div>
				</li>
			</ul>
			<form action="#" class="form_comment">
				<input type="text" placeholder="Add a comment or a reply here ..." class="input">
				<input type="submit" value="Send" class="btn">
			</form>
		</div>
	</div>
	<div class="clearfix pt20"></div>
</div>
<?php include "includes/footer.php";?>
</body>

</html>