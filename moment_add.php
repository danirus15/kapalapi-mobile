<html>
<?php include "includes/head.php";?>
<body class="bg_moment">
<?php include "includes/header.php";?>
 <div class="bg_float bg_moment1"></div>
<div class="content">

	<div class="moment_add_box">
        <form action="#">
            <div class="foto ">
            	<div class="name fl">Nama</div>
	            <div class="time fr">21.30</div>
	            <div class="clearfix"></div>
                <div class="pic imgLiquid imgLiquid_bgSize imgLiquid_ready" style="background-image: url(http://localhost/dn/awal/kapalapi/mobile/desktop/assets/avatar1.jpg); background-size: cover; background-position: 50% 50%; background-repeat: no-repeat;">
                    <img src="img/user_default.jpg" alt="" style="display: none;">
                </div>
                Filetype:jpg, png, Max size 3Mb
            </div>
            <div class="clearfix"></div>
            <a href="#" class="btn_take">Take Photo</a>
            <div class="clearfix"></div>
            <div class="line_or"><span>OR</span></div>
            <div class="clearfix"></div>
            <label class="input_file">
                <span class="browse">Browse</span>
                <input type="file">
            </label>
            <div class="clearfix pt10"></div>
            <div class="share_m">
                <label for="">
                    <input type="checkbox"> 
                    <span>Share to Facebook</span>
                </label>
                <label for="">
                    <input type="checkbox"> 
                    <span>Share to Pinterest</span>
                </label>
                <label for="">
                    <input type="checkbox"> 
                    <span>Share to Twitter</span>
                </label>
                <label for="">
                    <input type="checkbox"> 
                    <span>Share to Google +</span>
                </label>
            </div>
            <div class="clearfix pt30"></div>
            <input type="text" class="input_full" placeholder="Type your title here">
            <textarea name="" class="input_full input_text" placeholder="Type your descriptions here"></textarea>
            <span>140 Character Left</span>
            <input type="submit" value="Post" class="btn_post fr">
        </form>
        <div class="clearfix"></div>
    </div>
	<div class="clearfix pt20"></div>
</div>
<?php include "includes/footer.php";?>
</body>

</html>